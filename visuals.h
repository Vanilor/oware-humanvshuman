#pragma once

#include "lib.h"
#include "SDL2/include/SDL.h"
#include "SDL2/include/SDL_ttf.h"

void render(Hole** gamePlate, Player** players);
void colorConsole(int backgroundColor);
int calcTextColor(int backgroundColor);
void SDL_Custom_GenerateText(SDL_Renderer* renderer, int x, int y, char* text,
	TTF_Font* font, SDL_Texture** texture, SDL_Rect* rect);