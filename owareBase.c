#include "owareBase.h"
#include "lib.h"


#include <stdio.h>

void update(Hole** gamePlate, Player** players, short currentPlayerID, short* victory) {

	setCurrentPlayer(players, currentPlayerID);
	Player* player2 = get2ndPlayer(players);
	Player* currentPlayer = getCurrentPlayer(players);

	short player2Starving = isStarving(player2);

	if (player2Starving)
		printf("Votre adversaire n'a plus de pions, faites un mouvement pour lui en donner ! \n");

	/*
	*	Using loop instead of do{}while() because of C magic
	*
	*/
loop:
	printf("Joueur %hd, depuis quelle case souhaitez-vous �grener ? (Choisir entre 1 et 6) \n", currentPlayer->id);
	short choosenHoleRelativeID = 0, holeID;
	
	scanf_s("%hd", &choosenHoleRelativeID);
	emptyBuffer();

	if ((choosenHoleRelativeID > 6) || (choosenHoleRelativeID < 1))
		goto loop;

	/*
	*	We retrieve the real ID as it's ste in gamePlate array
	*/
	holeID = currentPlayerID * 6 + choosenHoleRelativeID - 1;

	/*
	*	Set the pawn amount of source hole to 0
	*/

	Hole* sourceHole = getHole(gamePlate, holeID);

	if (sourceHole->pawnsAmount == 0) {
		printf("Vous ne pouvez pas prendre les graines d'une case vide ! \n");
		goto loop;
	}

	currentPlayer->hand = (*sourceHole).pawnsAmount;

	sourceHole->pawnsAmount = 0;

	/*
	*	Incrementing it because we want to sow from the next hole
	*/
	holeID++;

	/*
	*	Handling too large index to avoid NPE
	*/
	if (holeID >= 12)
		holeID %= 12;

	movePawns(gamePlate, players, getHole(gamePlate, holeID));

	/**
	*
	*	We check if player2 is starving after pawns move
	*/
	if (player2Starving && isStarving(player2)) {
		
		/*
		*	We give the 2nd player all the pawns left on the game plate
		*/
		for (short i = 0; i < 2 * holesNb; i++)
			giveFromHole(player2, gamePlate[i]);

		victory = 1;
		return;
	}

	return;


}

void movePawns(Hole** gamePlate, Player** players, Hole* hole) {

	Player* currentPlayer = getCurrentPlayer(players);
	short sourceHoleID = hole->id;

	while (isPawnsInHand(currentPlayer)) {
		
		(hole->pawnsAmount)++;
		(currentPlayer->hand)--;

		short nextHole = hole->id + 1;
		if (nextHole == sourceHoleID)
			nextHole++;

		if (nextHole >= 12)
			nextHole %= 12;

		hole = getHole(gamePlate, nextHole);
	}

	if((hole->pawnsAmount == 2) || (hole->pawnsAmount == 3))
		takePawns(gamePlate, players, hole);

	return;
}

void takePawns(Hole** gamePlate, Player** pls, Hole* hole) {

	Player* currentPlayer = getCurrentPlayer(pls);
	Player* secondPlayer = get2ndPlayer(pls);

	if (!isPlayerOwningHole(currentPlayer, hole)){

		currentPlayer->capturedPawnsRound += hole->pawnsAmount;

		if (currentPlayer->capturedPawnsRound == secondPlayer->pawnsLeft) {
			printf("Vous ne pouvez pas prendre tous les pions de l'adversaire \n");
			currentPlayer->capturedPawnsRound = 0;
		}

		else {
			giveFromHole(currentPlayer, hole);
			short previousHoleIDBuffer = hole->id;

			/*
			*	Handling the case where holeID = 0
			*/
			if (previousHoleIDBuffer == 0)
				previousHoleIDBuffer = 12;

			return takePawns(gamePlate, pls, getHole(gamePlate, previousHoleIDBuffer--));
		}

	}

	else {
		printf("Vous avez pris %hd pions � votre adversaire ! \n", currentPlayer->capturedPawnsRound);
		printf("Vous avez captur� %hd pions au total \n", currentPlayer->capturedPawns);

		/*
		*	Resetting the round captured pawns amount
		*/
		currentPlayer->capturedPawnsRound = 0;
	}
	
	return;

}

void checkVictory(Player** players, short* victory) {

	if (victory == 1)
		return 1;

	else if (getCurrentPlayer(players)->capturedPawns >= 25 || get2ndPlayer(players)->capturedPawns >= 25)
		return 1;

	return 0;

}