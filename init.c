#include "init.h"

#include <stdlib.h>
#include <string.h>

Hole** createGamePlate() {

	Hole** gamePlate = (Hole**)calloc(2 * holesNb, sizeof(Hole*));
	if (!gamePlate)
		return NULL;

	for (short i = 0; i < 2 * holesNb; i++) {
		gamePlate[i] = (Hole*)calloc(1, sizeof(Hole));
		if (!gamePlate[i])
		{
			for (size_t j = 0; j < i; j++)
				free(gamePlate[j]);
			free(gamePlate);
			return NULL;
		}
		gamePlate[i]->id = i;
		gamePlate[i]->pawnsAmount = 4;
	}

	return gamePlate;

}

Player* createPlayer(Hole** gamePlate, short id) {
	Player* player = calloc(1, sizeof(Player));
	if (!player)
		return NULL;

	player->id = id;
	strcpy_s(player->name, nameLength, "Richard");
	player->color = 4;
	player->capturedPawns = 0;
	player->capturedPawnsRound = 0;
	player->holes = (Hole**)calloc(holesNb, sizeof(Hole*));
	if (!player->holes)
		return NULL;

	short firstHole = 6 * id;
	for (short i = 0; i < holesNb; i++)
		player->holes[i] = gamePlate[firstHole + i];

	player->pawnsLeft = 24;

	return player;
}