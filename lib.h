#pragma once

#include <SDL.h>
#include <SDL_ttf.h>

#define holesNb 6
#define nameLength 20

typedef struct Hole {

	short id;
	short pawnsAmount;

} Hole;

typedef struct Player {

	short id;
	char name[nameLength];
	char color;
	Hole** holes;
	short capturedPawns;
	short capturedPawnsRound;
	short pawnsLeft;
	short hand;
	short isCurrent;

} Player;

Player* get2ndPlayer(Player** players);
Player* getCurrentPlayer(Player** players);
Hole* getHole(Hole** gamePlate, short id); 
void setCurrentPlayer(Player** players, short currentPlayer);
short isPawnsInHand(Player* pl);
short calcPawnsLeft(Player* pl);
short isStarving(Player* pl);
void giveFromHole(Player* pl, Hole* hole);
short isPlayerOwningHole(Player* pl, Hole* hole);
void emptyBuffer();
void terminateMenu(SDL_Window* window, SDL_Renderer* renderer, TTF_Font* font);