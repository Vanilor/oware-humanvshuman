#include "lib.h"

#include <SDL.h>
#include <SDL_ttf.h>
#include <stddef.h>
#include <stdio.h>

/**
*	Need improvement
*/
Player* get2ndPlayer(Player** players) {

	if (players[0]->isCurrent)
		return players[1];
	return players[0];
}

Player* getCurrentPlayer(Player** players) {

	if (players[0]->isCurrent)
		return players[0];
	return players[1];

}

Hole* getHole(Hole** gamePlate, short id) {

	if (id < 0 || id > 12)
		return NULL;
	return gamePlate[id];
}

void setCurrentPlayer(Player** players, short currentPlayer) {

	/*
	*	Mathematical transcription of :
	*
	*	if(currentPlayer == 0) {
	*		players[0]->isCurrent = 1;
	*		players[1]->isCurrent = 0;
	*	}
	*	else {
	*		players[0]->isCurrent = 0;
	*		players[1]->isCurrent = 1;
	*	}
	*/
	players[0]->isCurrent = (-1) * currentPlayer + 1;
	players[1]->isCurrent = (-1) * currentPlayer + 1;

	return;
}

short isPawnsInHand(Player* pl) {
	if (pl->hand > 0)
		return 1;
	return 0;
}

short calcPawnsLeft(Player* pl) {

	for (short i = 0; i < holesNb; i++)
		pl->pawnsLeft += pl->holes[i]->pawnsAmount;

	return;
}

short isStarving(Player* pl) {

	if (calcPawnsLeft(pl) == 0)
		return 1;
	return 0;

}

void giveFromHole(Player* pl, Hole* hole) {

	pl->capturedPawns += hole->pawnsAmount;
	hole->pawnsAmount = 0;
	pl->pawnsLeft = calcPawnsLeft(pl);

	return;
}

short isPlayerOwningHole(Player* pl, Hole* hole) {

	for (short i = 0; i < holesNb; i++)
		if (pl->holes[i]->id == hole->id)
			return 1;
	return 0;

}

void emptyBuffer()
{
	int c = 0;
	while (c != '\n' && c != EOF)
	{
		c = getchar();
	}
}

void terminateMenu(SDL_Window* window, SDL_Renderer* renderer, TTF_Font* font) {

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	TTF_CloseFont(font);
	TTF_Quit();
	SDL_Quit();

	return;
}