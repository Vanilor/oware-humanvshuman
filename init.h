#pragma once

#include "lib.h"

Hole** createGamePlate();
Player* createPlayer(Hole** gamePlate, short id);