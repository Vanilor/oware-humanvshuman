#include "visuals.h"
#include "SDL2/include/SDL.h"
#include "SDL2/include/SDL_ttf.h"

#include <stdio.h>
#include <windows.h>


/**

Codes couleurs pour colorConsole :

0: noir
1: bleu fonc�
2: vert
3: bleu-gris
4: marron
5: pourpre
6: kaki
7: gris clair
8: gris
9: bleu
10: vert fluo
11: turquoise
12: rouge
13: rose fluo
14: jaune fluo
15: blanc

**/

void render(Hole** gamePlate, Player** players)
{
	for (short i = 0; i < 2 * holesNb; i++)
	{
		if (i == 0 || i == 6)
		{
			printf("\n\nJoueur %hd :  ", players[i / 6]->id);
		}

		if (i >= 6)
			colorConsole(12);
		else
			colorConsole(9);
		printf("  ");

		if(i < 6)
			printf("% 2hd  ", gamePlate[i]->pawnsAmount);
		else
			//Invert ID displayed from left to right because of rotational direction
			printf("% 2hd  ", gamePlate[17-i]->pawnsAmount); 

		colorConsole(0);

	}
	printf("\n\n");

	return;
}

void SDL_Custom_GenerateText(SDL_Renderer* renderer, int x, int y, char* text,
	TTF_Font* font, SDL_Texture** texture, SDL_Rect* rect) {

	SDL_Surface* surface;
	SDL_Color white = { 255, 255, 255, 0 };
	SDL_Color black = { 0,0,0,0 };

	surface = TTF_RenderText_Shaded(font, text, white, black);
	*texture = SDL_CreateTextureFromSurface(renderer, surface);

	SDL_FreeSurface(surface);

	rect->x = x;
	rect->y = y;
	rect->w = (*surface).w;
	rect->h = (*surface).h;

	SDL_RenderCopy(renderer, *texture, NULL, rect);

	return;
}

void colorConsole(int backgroundColor) {
	HANDLE H = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(H, backgroundColor * 16 + calcTextColor(backgroundColor));
}

int calcTextColor(int backgroundColor) {

	int textColor;

	switch (backgroundColor) {
	case 0:
		textColor = 15;
		break;
	case 1:
		textColor = 15;
		break;
	case 2:
		textColor = 0;
		break;
	case 3:
		textColor = 15;
		break;
	case 4:
		textColor = 15;
		break;
	case 5:
		textColor = 15;
		break;
	case 6:
		textColor = 15;
		break;
	case 7:
		textColor = 15;
		break;
	case 8:
		textColor = 15;
		break;
	case 9:
		textColor = 15;
		break;
	case 10:
		textColor = 0;
		break;
	case 11:
		textColor = 0;
		break;
	case 12:
		textColor = 15;
		break;
	case 13:
		textColor = 0;
		break;
	case 14:
		textColor = 0;
		break;
	case 15:
		textColor = 0;
		break;
	default:
		textColor = 15;
		break;
	}

	return textColor;

}