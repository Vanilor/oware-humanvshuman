#include "mainConsole.h"

/*
*	THIS FIRST CONSOLE VERSION, WITHOUT SDL, IS USING
*	BETWEEN 660kB AND 670kB OF MEMORY AT RUNTIME
*
*/

int mainConsole(int argc, char* argv[]) {

	/**
	*	Variables initialisation
	*
	*/
	Hole** gamePlate = createGamePlate();

	Player** players = (Player**)calloc(2, sizeof(Player*));
	if (!players)
		return 0;

	players[0] = createPlayer(gamePlate, 0);
	players[1] = createPlayer(gamePlate, 1);

	int round;
	char currentPlayer;
	round = currentPlayer = 0;
	short victory = 0;

	/*
	*	First render in order to show the game plate a first time before playing
	*/
	render(gamePlate, players);

	while (1) {

		currentPlayer = round%2;

		update(gamePlate, players, currentPlayer, &victory);		
		checkVictory(players, &victory);

		if (victory)
			break;
		else
			render(gamePlate, players);
		round++;
	}

	printf("Victoire !");
	return 0;

}