#pragma once

#include "lib.h"

void update(Hole** gamePlate, Player** players, short currentPlayer, short* victory);
void movePawns(Hole** gamePlate, Player** players, Hole* hole);
void takePawns(Hole** gamePlate, Player** pls, Hole* hole);
void checkVictory(Player** players, short* victory);