#include "visuals.h"
#include "SDL2/include/SDL.h"
#include "SDL2/include/SDL_ttf.h"
#include "mainConsole.h"
#include "mainSDL.h"

#include <stdio.h>

#define WIDTH 640
#define HEIGHT 480
#define APP_NAME "Awele HumanVsHuman - Matthieu HERBETTE"

int main(int argc, char* argv[]) {

    /*
    *	Set UTF-8 char encoding
    */
    SetConsoleOutputCP(1252);

    /*
    *   SDL inititialisations
    */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stdout, "[ERREUR] �chec de l'initialisation de la SDL (%s)\n", SDL_GetError());
        return 0;
    }

    if (TTF_Init() == -1) {
        fprintf(stderr, "[ERREUR]  �chec de l'initialisation du TTF (%s)\n", TTF_GetError());
        return 0;
    }

    /*
    *   Variables declaration
    */

    /*
    *   Window-related vars
    */
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_Event event;

    /*
    *   Text-related vars
    */
    TTF_Font* font = TTF_OpenFont("SDL2\\RobotoCondensed.ttf", 32);
    SDL_Rect pos;
    SDL_Texture* titleTexture = NULL;
    SDL_Texture* choiceTexture = NULL;
    SDL_Texture* terminateTexture = NULL;

    short terminate = 0;

    /*
     *   Application initialisation
     */

    if (!font) {
        fprintf(stdout, "[ERREUR] �chec de l'initialisation de la police TTF, v�rifiez le chemin relatif");
        return 0;
    }

    window = SDL_CreateWindow(APP_NAME,
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        WIDTH,
        HEIGHT,
        SDL_WINDOW_SHOWN
    );
    if (!window) {
        fprintf(stderr, "Erreur de cr�ation de la fen�tre: %s\n", SDL_GetError());
        return 0;
    }

    renderer = SDL_CreateRenderer(window, -1, 0);
    if (!renderer) {
        fprintf(stderr, "Erreur de cr�ation du renderer : %s\n", SDL_GetError());
        return 0;
    }

    while (!terminate) {

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);

        SDL_WaitEvent(&event);
        switch (event.type) {
            
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {

                    case SDLK_ESCAPE:

                        SDL_Custom_GenerateText(renderer, 50, 100, "Vous avez quitt� l'application, au revoir !", font, &terminateTexture, &pos);

                        SDL_RenderPresent(renderer);
                        SDL_Delay(2000);
                        terminate = 1;
                        break;
                    case SDLK_1:
                        terminateMenu(window, renderer, font);
                        mainConsole(argc, argv);
                        terminate = 1;
                        break;
                    case SDLK_2:
                        terminateMenu(window, renderer, font);
                        mainSDL(argc, argv);
                        terminate = 1;
                        break;
                    default:
                        break;
                }
                break;
            /*
            *   Unhandled event, ignoring it
            */
            default: 
                break;
        }

        SDL_Custom_GenerateText(renderer, 125, 50, "Choisissez le mode d'affichage", font, &titleTexture, &pos);
        pos.x = 50;
        pos.y = 100;
        SDL_Custom_GenerateText(renderer, 50, 100, "Appuyez sur 1 pour Console, 2 pour SDL", font, &choiceTexture, &pos);

        SDL_RenderPresent(renderer);

    }

    terminateMenu(window, renderer, font);
    return 1;
}